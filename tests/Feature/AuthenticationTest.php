<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    //test if validation works for registration endpoint
    public function testRequiredFieldsForRegistration()
    {
        $response = $this->json('POST', 'api/register', ['Accept' => 'application/json'])
            ->assertStatus(422);
    }
    // test if !conformed password returns validatoion message
    public function testPasswordConfirmation()
    {
        $userData = [
            "name" => "Chucks",
            "email" => "chucks@gmail.com",
            "password" => "demo12345"
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => ["The password confirmation does not match."]
                ]
            ]);
    }

    public function testSuccessfulRegistration()
    {
        $userData = [
            "name" => "Chucks",
            "email" => "chucks@gmail.com",
            "password" => "secret",
            "password_confirmation" => "secret"
        ];

        $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(200);
           
    }



    public function testSuccessfulLogin()
    {
        $user = factory(User::class)->create([
           'email' => 'chucks@gmail.com',
           'password' => bcrypt('secret'),
        ]);


        $loginData = ['email' => 'chucks@gmail.com', 'password' => 'secret'];

        $this->json('POST', 'api/login', $loginData, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
    // public function testifauthmiddlewareworks(){
    //     $this->json('GET', 'api/user', ['Accept' => 'application/json'])
    //     ->assertStatus(403);
    // }
}
